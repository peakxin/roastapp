<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Web\AppController@getApp')
    ->middleware('auth');
//Route::get('/', 'Web\AppController@getApp');
// guest 中间件，该中间件的用途是登录用户访问该路由会跳转到指定认证后页面，而非登录用户访问才会显示登录页面
Route::get('/login', 'Web\AppController@getLogin' )
    ->name('login')
    ->middleware('guest');
Route::get('/auth/{social}', 'Web\AuthenticationController@getSocialRedirect')
    ->middleware('guest');
Route::get('/auth/{social}/callback', 'Web\AuthenticationController@getSocialCallback')
    ->middleware('guest');
